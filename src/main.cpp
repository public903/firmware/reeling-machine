
#include <Arduino.h>
#include <LiquidCrystal.h>
// #include <DRV8825.h>
#include "config.h"
#include <Keypad.h>
#include "stepper.h"

// #define DEBUG
// --------------------------------------------------------------------------------------
LiquidCrystal lcd(DRS, DEN, DiD4, DiD5, DiD6, DiD7);
STEPPER_DRV8825 motor;
Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
// --------------------------------------------------------------------------------------
// Variables
const uint8_t rpmTab[] = {10,20,40,60,120};
int16_t revCoil, revPas;
uint8_t rti = 3, rpm = rpmTab[rti], stri=0;
bool led, mrun_tape=false;
int32_t stepsCounter = 0, lastStepsCount;
enum mode_t {NORMAL, EDIT_REV_FIN, TAPE} mode;
char customKey;
char str[5];
uint32_t tPrevious = 0, wait_time_micros;
const uint16_t timer1s = 1000;           // interval LED blink
const uint8_t timerLED = 100;
//--------------------------------------------------------------------------------------
#define PRINTEDIT lcd.setCursor(0,0); \
                  lcd.print(txtEdit);
#define PRINTRPM  lcd.setCursor(0,1); \
                  lcd.print(txtRpm); \
                  lcd.print(rpm); \
                  for (int8_t i=9-sizeof(txtRpm)-sizeof(rpm); i>0; i--) lcd.print(" ");
#define PRINTDIR  lcd.setCursor(0,2); \
                  lcd.print(txtDir); \
                  if (motor.getDir()) lcd.print("+"); \
                  else lcd.print("-"); 
#define PRINTMODE  lcd.setCursor(0,3); \
                if (mode == NORMAL) lcd.print(txtModeCoil); \
                if (mode == TAPE) lcd.print(txtModeTape); \
                if (mode == EDIT_REV_FIN) lcd.print(txtModeEdit);
#define PRINTREVCOIL  lcd.setCursor(10,0); \
                  lcd.print(txtRevCoil); \
                  lcd.print(revCoil); \
                  for (int8_t i=9-sizeof(txtRevCoil)-sizeof(revCoil); i>0; i--) lcd.print(" ");
#define PRINTREVPAS  lcd.setCursor(10,1); \
                  lcd.print(txtRevPas); \
                  revPas = stepsCounter / (MOTOR_STEPS * MICROSTEPS); \
                  lcd.print(revPas); \
                  for (int8_t i=9-sizeof(revPas)-sizeof(txtRevPas); i>0; i--) lcd.print(" ");
#define PRINTENTER  lcd.setCursor(10,2); \
                  lcd.print(txtEnter); 
#define PRINTESC  lcd.setCursor(10,3); \
                  lcd.print(txtEsc); 
#define CURSORFIN lcd.setCursor(sizeof(txtRevCoil)+9,0);

#define STRCLEAR  for (stri=4;stri >0;stri--) str[stri] = 0;

#define RESETSTEPSCOUNTER stepsCounter = 0; \
                          lastStepsCount = motor.getStepsCounter();

//--------------------------------------------------------------------------------------
bool getFootSw(void);
//--------------------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  #ifdef DEBUG
    Serial.println("Reeling Machine debuging");
  #endif
  motor.begin(ENABLE_PIN, DIR_PIN, STEP_PIN);
  motor.setRpm(rpm);
  motor.setDir(true);

  lcd.begin(20, 4);
  PRINTEDIT;
  PRINTRPM;
  PRINTDIR;
  PRINTMODE;
  PRINTREVCOIL;
  PRINTREVPAS;
  PRINTENTER;
  PRINTESC;
}
//--------------------------------------------------------------------------------------
void loop() {
    motor.run();
    if (getFootSw()) {
      motor.setPause(false);
      if (mode == TAPE) {
        mrun_tape = true;
        if (!motor.getRun()) motor.start(MOTOR_STEPS * MICROSTEPS / 4);
      }
    }
    else {
      motor.setPause(true);
      if (mrun_tape) {
        mrun_tape = false;
        lastStepsCount = motor.getStepsCounter();
        motor.start(revCoil*MOTOR_STEPS*MICROSTEPS - stepsCounter);
      }
      if (lastStepsCount != motor.getStepsCounter()) {
        stepsCounter += motor.getStepsCounter() - lastStepsCount;
        lastStepsCount = motor.getStepsCounter();
        motor.start(revCoil*MOTOR_STEPS*MICROSTEPS - stepsCounter);
        PRINTREVPAS;
      }
    }

    // Casovac 1s
    if (millis() - tPrevious >= timer1s) {
      tPrevious = millis();
      digitalWrite(LED_BUILTIN, 1); led = true;
      if (!motor.getRun() && (mode==NORMAL || mode==TAPE)) {
      //  PRINTREVPAS;
      #ifdef DEBUG
      Serial.print("Steps counter=");
      Serial.println(motor.getStepsCounter());
      Serial.print("Motor run=");
      Serial.println(motor.getRun());
      Serial.print("Slapka=");
      Serial.println(analogRead(A6));
      #endif
      }
    }
    // Casovac 100ms pre bliknutie LEDky
    if ((millis() - tPrevious >= timerLED) && led) { // LED blink on 100ms
      digitalWrite(LED_BUILTIN, 0); led = false;
    }

  if (!motor.getRun()) {
    customKey = kpd.getKey();  // Keypad
    if (customKey) {
      #ifdef DEBUG
        Serial.println(customKey);
      #endif
      switch (customKey)
      {
      case 'A':
        if (mode == NORMAL || mode == TAPE) mode = EDIT_REV_FIN;
        else if (mode == EDIT_REV_FIN) {
          revCoil = atoi(str);
          PRINTREVCOIL;
          mode = NORMAL;
        }
        PRINTMODE;
        #ifdef DEBUG
        Serial.println(mode);
        #endif
        switch (mode)
        {
        case NORMAL:
          lcd.noBlink();
          STRCLEAR;
          RESETSTEPSCOUNTER;
          PRINTREVPAS;
          motor.start(revCoil * MOTOR_STEPS * MICROSTEPS);
          break;
      
        case EDIT_REV_FIN:
          CURSORFIN;
          lcd.blink();
          break;
      
        default:
          break;
        }
        break;
    
      case 'B':
        if (mode == NORMAL || mode == TAPE) {
          if (++rti >= 5) rti = 0;
          rpm = rpmTab[rti];
          PRINTRPM;
          motor.setRpm(rpm);
        }
        break;
    
      case 'C':
        if (mode == NORMAL || mode == TAPE) {
          if (motor.getDir()) motor.setDir(false);
          else motor.setDir(true);
          PRINTDIR;
        }
        break;
    
      case 'D':
        if (mode != NORMAL) mode = NORMAL;
        else mode = TAPE;
        PRINTMODE;
        break;
    
      case '0':
      case '9':
      case '8':
      case '7':
      case '6':
      case '5':
      case '4':
      case '3':
      case '2':
      case '1':
        switch (mode)
        {
        case NORMAL:
            /*
            motor.start(rev * MOTOR_STEPS * MICROSTEPS);
            mrun_tape = true;
            motor.setPause(false);
            #ifdef DEBUG
              Serial.print("DIR=");Serial.println(motor.getDir());
              Serial.print("REV=");Serial.println(rev);
              Serial.println("Motor START");
            #endif
            break;
            */
          break;
        case EDIT_REV_FIN:
          if (stri < 4) 
          {
            lcd.print(customKey);
            str[stri] = customKey;
            stri++;
          }
          break;
        }
        break;

      case '*':
        switch (mode)
        {
        case EDIT_REV_FIN:
          PRINTREVCOIL;
          break;
        case NORMAL:
          break;
        }
        STRCLEAR;
        mode = NORMAL;
        PRINTMODE;
        lcd.noBlink();
        break;
    
      case '#':
        switch (mode)
        {
        case EDIT_REV_FIN:
          revCoil = atoi(str);
          PRINTREVCOIL;
          break;
        case NORMAL:
          break;
        }
        lcd.noBlink();
        mode = NORMAL;
        PRINTMODE;
        STRCLEAR;
        RESETSTEPSCOUNTER;
        PRINTREVPAS;
        motor.start(revCoil * MOTOR_STEPS * MICROSTEPS);
        break;
    
      default:
        break;
      }
    }
  }
}
//--------------------------------------------------------------------------------------
bool getFootSw(void) {
  if (analogRead(FOOTSW_PIN) > 512) return(false);
  else return(true);
}