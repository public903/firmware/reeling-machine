/* 
 * Library for stepper motor driver DRV8825
 * 
 * MOTOR STEPS = 200
 * MICROSTEPS = 4
 * WIRE: ENABLE, DIR, STEP
 * RPM: 6 - 120
 * 
 * Author: Trnik 23.7.2020
 * 
*/
#include "stepper.h"
#include <Arduino.h>

//-------------------------------------------------------------
STEPPER_DRV8825::STEPPER_DRV8825() {
}
//-------------------------------------------------------------
void STEPPER_DRV8825::begin(uint8_t enable, uint8_t dir, uint8_t step, uint16_t mot_stps = 200, uint8_t micstps = 4) {
    pinEn = enable;
    pinDir = dir;
    pinSt = step;
    motorSteps = mot_stps;
    microsteps = micstps;
    setRpm(60);
    pinMode(pinEn, OUTPUT);
    pinMode(pinDir, OUTPUT);
    pinMode(pinSt, OUTPUT);
    digitalWrite(pinEn, HIGH);
    digitalWrite(pinDir, HIGH);
    digitalWrite(pinSt, LOW);
    wait_micros = 0;
    move = 0;
}
//-------------------------------------------------------------
void STEPPER_DRV8825::run(void) {
  #ifdef DEBUG
    Serial.println("Motor ENABLED");
  #endif
  if ((move > 0) && (!pause)) {
    digitalWrite(pinEn, LOW);
    if (dir) digitalWrite(pinDir,HIGH);
    else digitalWrite(pinDir, LOW);
    if ((micros() - wait_micros) >= stepInterval) {
        digitalWrite(pinSt,HIGH);
        wait_micros = micros();
        delayMicroseconds(8);
        digitalWrite(pinSt,LOW);
        move--;
        if (dir) counter++;
        else counter--;
    }
  }
  else digitalWrite(pinEn, HIGH);
  //if (!pause) digitalWrite(pinEn, HIGH);
}
//-------------------------------------------------------------
void STEPPER_DRV8825::setRpm(uint8_t rpm) {
    stepInterval = 10000000L / (motorSteps * microsteps * (rpm / 6));
}
//-------------------------------------------------------------
void STEPPER_DRV8825::start(uint32_t moveSteps) {
    move = moveSteps;
}
//-------------------------------------------------------------
void STEPPER_DRV8825::setPause(bool ppause) {
    pause = ppause;
}
//-------------------------------------------------------------
void STEPPER_DRV8825::stop(void) {
    move = 0;
}
//-------------------------------------------------------------
int32_t STEPPER_DRV8825::getStepsCounter(void) {
    return(counter);
}
//-------------------------------------------------------------
void STEPPER_DRV8825::resetCounter(void) {
    counter = 0;
}
//-------------------------------------------------------------
void STEPPER_DRV8825::setDir(bool sdir) {
    dir = sdir;
}
//-------------------------------------------------------------
bool STEPPER_DRV8825::getDir(void) {
    return(dir);
}
//-------------------------------------------------------------
bool STEPPER_DRV8825::getRun(void) {
    return((move > 0) && !pause);
}
//-------------------------------------------------------------
