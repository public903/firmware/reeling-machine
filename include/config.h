#ifndef BOARD_H
#define BOARD_H

#include <Arduino.h>

// LCD
#define DRS 9
#define DEN 10
#define DiD4 8
#define DiD5 7
#define DiD6 6
#define DiD7 5

// MOTOR
#define MOTOR_STEPS 200L
#define DIR_PIN 2
#define STEP_PIN    3
#define ENABLE_PIN  4
#define MICROSTEPS  4L

// KEYPAD 4x4
#include <Keypad.h>

// FOOT SWITCH
#define FOOTSW_PIN  A6

const byte ROWS = 4; 
const byte COLS = 4; 
char keys[ROWS][COLS] = {
{'1','2','3','A'},
{'4','5','6','B'},
{'7','8','9','C'},
{'*','0','#','D'}
};
byte rowPins[ROWS] = {12, 11, A0, A1}; //connect to the row pinouts of the kpd
byte colPins[COLS] = {A2, A3, A5, A4}; //connect to the column pinouts of the kpd

// TEXT
const char txtRevCoil[]="Coil=";
const char txtRevPas[]="Pas=";
const char txtEnter[]="#)Enter";
const char txtEsc[]="*)ESC";
const char txtEdit[]="A)Edit";
const char txtRpm[]="B)RPM=";
const char txtDir[]="C)DIR=";
const char txtModeCoil[]="D)COIL";
const char txtModeTape[]="D)TAPE";
const char txtModeEdit[]=" EDIT  ";

#endif  // BOARD_H