# Reeling machine

This machine is designed for winding small coils, transformers, etc.

#### Specification hardware

- [Arduino NANO](ArduinoNano.jpg)
- Stepper motor driver [DRV8825](drv8825.png)
- [Keypad 4x4](keypad4x4.jpg)
- [Foot switch](footsw.jpg)
- [LCD 2x16](lcd2x16.jpg)
- stepper motor [NEMA17](nema17.jpg)